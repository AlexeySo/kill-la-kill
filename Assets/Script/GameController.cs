﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/**
 * Класс GameController для изменения параметров игрока
 * 
 * @autor Sobolev A.A.
 */
public class GameController : MonoBehaviour
{

    public static GameController instance;

   
    private static float moveSpeed = 5f;
    private static float fireRate = 0.5f;
    private static float bulletSize = 0.5f;

    private bool bootCollected = false;
    private bool screwCollected = false;

    public List<string> collectedNames = new List<string>();

 
    public static float MoveSpeed { get => moveSpeed; set => moveSpeed = value; }
    public static float FireRate { get => fireRate; set => fireRate = value; }
    public static float BulletSize { get => bulletSize; set => bulletSize = value; }

    public Text healthText;

    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    /**
     * Метод для изменения скорости игрока
     */
    public static void MoveSpeedChange(float speed)
    {
        moveSpeed += speed;
    }
    /**
     * Метод для изменения скорости стрелбы игрока
     *
     *@param rate изменение скорости стрельбы от предметов
     */
    public static void FireRateChange(float rate)
    {
        fireRate -= rate;
    }
    /**
     * Метод для изменения размера стрельбы игрока
     */
    public static void BulletSizeChange(float size)
    {
        bulletSize += size;
    }
    /**
     * Метод для создания синергии предметов
     */
    public void UpdateCollectedItems(CollectionController item)
    {
        collectedNames.Add(item.item.name);

        foreach (string i in collectedNames)
        {
            switch (i)
            {
                case "Boot":
                    bootCollected = true;
                    break;
                case "Screw":
                    screwCollected = true;
                    break;
            }
        }

        if (bootCollected && screwCollected)
        {
            FireRateChange(0.25f);
        }
    }



}