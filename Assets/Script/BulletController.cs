﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 * Класс для создания пули
 * 
 * @autor Sobolev A.A.
 */
public class BulletController : MonoBehaviour
{
    public float lifeTime;

    // Запуск вызывается перед первым обновлением кадра
    void Start()
    {
        StartCoroutine(DeathDelay());
        transform.localScale = new Vector2(GameController.BulletSize, GameController.BulletSize);
    }

    IEnumerator DeathDelay()
    {
        yield return new WaitForSeconds(lifeTime);
        Destroy(gameObject);
    }

   
}