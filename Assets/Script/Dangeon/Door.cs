﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 *Класс для реализации двери 
 * 
 * @autor  Trofimov V.V
 * 
 */
public class Door : MonoBehaviour
{
    Animator anim;
    Collider2D collision;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        col = GetComponent<Collider2D>();
    }
    //состояние двери 
    public void Open()
    {
        anim.SetBool("Open", true);
    }
    public void Close()
    {
        anim.SetBool("Open", false);
    }
    public void Enable()
    {
        col.enabled - true;
    }
    public void Disable()
    {
        col.enable = false;
    }
}