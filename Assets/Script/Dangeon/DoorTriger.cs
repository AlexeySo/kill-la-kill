﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/**
 *Класс для  открытия двери 
 * 
 * @autor  Trofimov V.V
 * 
 */
public class DoorTriger : MonoBehaviour
{
    public Door door;
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            door.Open();
        }
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            door.Close();
        }
    }
}